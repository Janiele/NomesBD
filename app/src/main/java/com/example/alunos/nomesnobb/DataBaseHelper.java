package com.example.alunos.nomesnobb;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alunos on 18/04/2018.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Empregados.db";
    public static final String TABLE_NAME = "Empregados_table";

    public static final String COL_1 = "ID";
    public static final String COL_2 = "NOME";
    public static final String COL_3 = "SOBRENOME";
    public static final String COL_4 = "PROFISSÃO";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE " +TABLE_NAME +
                " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOME TEXT, SOBRENOME TEXT, PROFISSAO TEXT)" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP  TABLE IF EXISTS " + TABLE_NAME);

    }
    public boolean insertData(String nome, String sobrenome, String profissao){

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();//criando objeto p poder escrever o que vai ser digitado na tabela de base de dados
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, nome);
        contentValues.put(COL_2, sobrenome);
        contentValues.put(COL_2, profissao);

        long result = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);//nova variavel inserir novas informações
        sqLiteDatabase.close();

        if ((result== -1)) return false;
        else return true;




    }
}
