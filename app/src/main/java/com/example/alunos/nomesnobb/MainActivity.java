package com.example.alunos.nomesnobb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText txtNome;
    EditText txtSobrenome;
    EditText txtProfissao;
    Button btnEnviar;
    
    DataBaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     
        myDb = new DataBaseHelper(this);
        
         txtNome = (EditText)findViewById(R.id.editTextName);
         txtSobrenome = (EditText)findViewById(R.id.editTextSobrenome);
         txtProfissao = (EditText)findViewById(R.id.editTextProfissao);
         btnEnviar = (Button)findViewById(R.id.buttonEnviar);
        
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
            }
        });
    }
    public void ClickMe(){
        String nome = txtNome.getText().toString();
        String sobrenome = txtSobrenome.getText().toString();
        String profissao = txtProfissao.getText().toString();
        
        Boolean result = myDb.insertData(nome, sobrenome, profissao);

        if (result== true) Toast.makeText(this, "Dados inseridos com sucesso", Toast.LENGTH_SHORT).show();
        else Toast.makeText(this, "Erro ao inserir dados", Toast.LENGTH_SHORT).show();
    }
}
